var menuState = {
    create: function () {
        // Add a background image
        this.background=game.add.image(0, 0, 'bg');
        this.background.scale.setTo(1.5, 1.5); 
        // Display the name of the game
        var nameLabel = game.add.text(game.width / 2, 80, 'Raiden',
            { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        game.global.name;
        // Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width / 2, game.height / 2,
            'LAST SCORE: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
        // Explain how to start the game
        var startLabel = game.add.text(game.width / 2, game.height - 80,
            'Choose your level between left and right key', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        var rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        leftKey.onDown.add(this.start, this);
        rightKey.onDown.add(this.start2, this);
    },
    start: function () {
        // Start the actual game
        game.global.name = prompt("Please Enter Your Name:", "name");
        if( game.global.name == null)  game.global.name= 'name';
        game.global.level=1;
        game.state.start('play');
    },
    start2: function () {
        // Start the actual game
        game.global.name = prompt("Please Enter Your Name:", "name");
        if( game.global.name == null)  game.global.name= 'name';
        game.global.level=2;
        game.state.start('play');
    }
}; 