var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width / 2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        game.load.image('player', 'Mari.png');
        game.load.image('enemy', 'ufo.png');
        //enemy.scale.setTo(0.5,0.5);
        game.load.image('coin', 'coin.png');
        game.load.image('wallV', 'wallV.png');
        game.load.image('wallH', 'ground.png');
        game.load.image('bullet1', 'bullet1.png');
        game.load.image('bullet2', 'bullet2.png');
        game.load.image('bg', 'background.png');
        game.load.image('stone', 'stone.jpg');
        game.load.image('ulti', 'ulti.png');
        game.load.image('fullmp', 'mp.png');
        game.load.image('helper', 'helper2.png');
        // Load a new asset that we will use in the menu state
        game.load.image('background', 'background.jpg');

        game.load.spritesheet('player', 'jet.png', 83, 60);
        game.load.spritesheet('coin_dead', 'coin_dead.png', 16, 16, 12);
        //game.load.spritesheet('enemy', 'enemy.png', 33, 35);
        game.load.audio('bullet_sound', 'Laser Gun Sound Effect.mp3');
        game.load.audio('game_sound', 'Lunatic Princess.mp3');
        game.load.audio('menu_bgm', 'menu bgm.mp3');    
    },
    create: function () {
        // Go to the menu state
        game.state.start('menu');
    }
}; 