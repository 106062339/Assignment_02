var weapon;
var enemyBulletTime = 0;
var livingEnemies = [];
var livingEnemys = [];
var playerShipExplosion;
var enemyShotSound;
var playerShotSound;
var pause;
var mute;

var CoinParticle = (function () {
    var CoinParticle = function (game, x, y) {
        Phaser.Particle.call(this, game, x, y, 'coin_dead');
        this.animations.add("rotate");
    };
    CoinParticle.prototype = Object.create(Phaser.Particle.prototype);
    CoinParticle.prototype.constructor = CoinParticle;
    CoinParticle.prototype.onEmit = function () {
        this.animations.stop("rotate", true);
        this.animations.play("rotate", 60, true);
        this.animations.getAnimation('rotate').frame = Math.floor(Math.random() * this.animations.getAnimation('rotate').frameTotal);
    }
    return CoinParticle;
}());

var playState = {
    preload: function () { }, // The proload state does nothing now.
    create: function () {
        //weapon&music
        this.lazer = game.add.audio('bullet_sound');
        this.game_sound = game.add.audio('game_sound');
        this.game_sound.loopFull();
        var _this = this;
        if (game.global.level == 1)
            this.bg = game.add.sprite(0, -1 * game.height, 'bg');
        else
            this.bg = game.add.sprite(0, -1 * game.height, 'stone');
        this.bg.width = game.width;
        this.bg.height = game.height * 2;
        game.physics.enable(this.bg, Phaser.Physics.ARCADE);
        //particle
        //this.game.input.onTap.add(this.createParticle, this);

        //keys
        this.keyP = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.keyO = game.input.keyboard.addKey(Phaser.Keyboard.O);
        this.keyM = game.input.keyboard.addKey(Phaser.Keyboard.M);
        this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.keyplus = game.input.keyboard.addKey(Phaser.Keyboard.K);
        this.keyminus = game.input.keyboard.addKey(Phaser.Keyboard.L);
        this.gamepause = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_MULTIPLY);
        this.mute = false;

        this.keyP.onDown.add(function () {
            _this.game_sound.pause();
            pause = true;
        });
        this.keyO.onDown.add(function () {
            _this.game_sound.resume();
            pause = false;
        });
        
        
        this.keyplus.onDown.add(function () {
            //console.log(game_sound.volume);
            _this.game_sound.volume += 0.1;
            if (_this.game_sound.volume > 1) _this.game_sound.volume = 1;
        });
        this.keyminus.onDown.add(function () {
            //console.log(game_sound.volume);
            _this.game_sound.volume -= 0.1;
            if (_this.game_sound.volume < 0) _this.game_sound.volume = 0;
        });



        game.global.pau = 0;
        game.global.score = 0;
        game.global.hp = 3;
        game.global.ultcnt = 0;
        game.global.counter=19;
        this.cursor = game.input.keyboard.createCursorKeys();
        this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');
        game.physics.arcade.enable(this.player);
        this.player.frame = 23;
        this.player.animations.add('no_walk', [0, 5], 2, true);
        this.player.animations.add('fly', [1, 2, 3, 4], 4, true);

//////////////////////////////
        this.helper = game.add.sprite(game.width +100, game.height+100, 'helper');
        game.physics.arcade.enable(this.helper);
        this.helper.body.velocity.y=0;
        this.helper.body.velocity.x=0;

///////////////////////////////
        this.mp = game.add.sprite(700, 500, 'ulti');
        this.mp.width = 3;
        this.mp.height = 50;
        this.fullmp = game.add.sprite(700, 500, 'fullmp');
        this.fullmp.width = 203;
        this.fullmp.height = 3;

        this.walls = game.add.group();
        this.walls.enableBody = true;
        //game.add.sprite(0, 0, 'wallV', 0, this.walls);
        this.walls.setAll('body.immovable', true);

        this.coin = game.add.sprite(60, 140, 'coin');
        game.physics.arcade.enable(this.coin);
        this.coin.anchor.setTo(0.5, 0.5);
        this.scoreLabel = game.add.text(700, 400, 'score: 0',
            { font: '30px Arial', fill: '#ffffff' });
        this.lifeLabel = game.add.text(700, 450, 'LIFE: ' + game.global.hp,
            { font: '30px Arial', fill: '#ffffff' });
        this.coinLabel = game.add.text(700, 600, 'COIN: ' + game.global.counter,
            { font: '30px Arial', fill: '#ffffff' });
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');
        if (game.global.level == 1)
            game.time.events.loop(2500, this.addEnemy, this);
        else
            game.time.events.loop(1500, this.addEnemy, this);
        ///////////////////////////////////
        /* this.enemys = game.add.group();
         this.enemys.enableBody = true;
         this.enemys.createMultiple(10, 'enemy');
         if(game.global.level==1)
             game.time.events.loop(2500, this.addEnemys, this);
         else 
             game.time.events.loop(1500, this.addEnemys, this);*/
        ////////////////////////////////////////////////


        //  Creates 1 single bullet, using the 'bullet' graphic
        if (game.global.level == 1)
            this.weapon = game.add.weapon(300, 'bullet1');
        else
            this.weapon = game.add.weapon(300, 'bullet1');
        //  The bullet will be automatically killed when it leaves the world bounds
        game.physics.arcade.enable(this.weapon);
        this.weapon.multifire=true;
        this.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        //  Because our bullet is drawn facing up, we need to offset its rotation:
        this.weapon.bulletAngleOffset = 90;
        //  The speed at which the bullet is fired
        this.weapon.bulletSpeed = 400;

        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;

        this.enemyBullets.createMultiple(30, 'bullet2');


        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        /////////////////////////////////////
        this.weap = game.add.weapon(15, 'bullet1');
        //  The bullet will be automatically killed when it leaves the world bounds
        game.physics.arcade.enable(this.weap);
        //this.weapon.multifire=true;
        this.weap.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        //  Because our bullet is drawn facing up, we need to offset its rotation:
        this.weap.bulletAngleOffset = 90;
        //  The speed at which the bullet is fired
        this.weap.bulletSpeed = 400;
        this.weap.trackSprite(this.helper, 35, 0);

        ////////////////
        /*    this.Bullets = game.add.group();
            this.Bullets.enableBody = true;
            this.Bullets.physicsBodyType = Phaser.Physics.ARCADE;
            
            this.Bullets.createMultiple(30, 'bullet2');
        
                
            this.Bullets.setAll('anchor.x', -1);
            this.Bullets.setAll('anchor.y', 0);
            this.Bullets.setAll('outOfBoundsKill', true);
            this.Bullets.setAll('checkWorldBounds', true);
    */

        ////////////////
        //  Tell the Weapon to track the 'player' Sprite, offset by 14px horizontally, 0 vertically 
        this.weapon.trackSprite(this.player, 35, 0);
        //weapon.trackSprite(this.enemies, 100, 600);
        cursors = this.input.keyboard.createCursorKeys();
        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        fireButton.onDown.add(function () {
            game.paused=false;

        });
    },
    update: function () {
        
            //console.log(this.enemies.countLiving());
            if (!this.player.inWorld) { this.playerDie2(); }
            if (!this.helper.inWorld) { this.helper.kill(); }
            game.physics.arcade.collide(this.player, this.floor);
            game.physics.arcade.collide(this.player, this.walls);
            game.physics.arcade.collide(this.enemies, this.walls);
            game.physics.arcade.collide(this.enemies, this.floor);
            game.physics.arcade.collide(this.enemies, this.weapon);
            // game.physics.arcade.collide(this.enemys, this.walls);
            // game.physics.arcade.collide(this.enemys, this.floor);
            // game.physics.arcade.collide(this.enemys, this.weapon);
            game.physics.arcade.overlap(this.weapon.bullets, this.enemies, this.EnemyDie, null, this);
            // game.physics.arcade.overlap(this.weapon.bullets, this.enemys, this.EnemyDie, null, this);
            //game.physics.arcade.overlap(this.weapon.bullets, this.enemies, this.createParticle_enemy, null, this);
            game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);
            game.physics.arcade.overlap(this.player, this.enemies, this.playerDie2, null, this);

            //game.physics.arcade.overlap(this.player, this.enemys, this.playerDie2, null, this);
            game.physics.arcade.overlap(this.enemyBullets, this.player, this.playerDie, null, this);
            // game.physics.arcade.overlap(this.enemysBullets, this.player, this.playerDie, null, this);
            
            game.physics.arcade.overlap(this.weap.bullets, this.enemies, this.EnemyDie, null, this);
            
            /////////////////////////////////////////////////////
            livingEnemies.length = 0;
            this.enemies.forEachAlive(function (enemy) {
                livingEnemies.push(enemy)
            });

            if (this.time.now > enemyBulletTime) {
                this.enemyBullet = this.enemyBullets.getFirstExists(false);
                if (this.enemyBullet && livingEnemies.length > 0) {

                    var random = this.rnd.integerInRange(0, livingEnemies.length - 1);
                    var shooter = livingEnemies[random];
                    this.enemyBullet.reset(shooter.body.x + 50, shooter.body.y + 100);
                    enemyBulletTime = this.time.now + 500;
                    if (game.global.level == 1)
                        this.enemyBullet.body.velocity.y = 500;
                    else
                        this.enemyBullet.body.velocity.y = 700;
                }
            }

            //////////
            /* livingEnemys.length = 0;
             this.enemys.forEachAlive(function (enemy2) {
                 livingEnemys.push(enemy2)
             });
             console.log(this.enemys);
             if (this.time.now > enemyBulletTime) {
                 this.Bullet = this.Bullets.getFirstExists(false);
                 if (this.enemysBullet && livingEnemys.length > 0) {
     
                     var random = this.rnd.integerInRange(0, livingEnemys.length - 1);
                     var shooter2 = livingEnemys[random];
                     this.Bullet.reset(shooter2.body.x, shooter2.body.y + 70);
                     enemyBulletTime = this.time.now + 300;
                     if(game.global.level==1)
                         this.Bullet.body.velocity.y = 300;
                     else
                         this.Bullet.body.velocity.y = 500;
                 }
             }*/
            /////////
            
    
                  
                    
             
            if(game.global.level==1) 
                this.bg.y += 10;
            else 
                this.bg.y +=30;
            if (this.bg.y >= -1) {
                this.bg.y = -1 * game.height;
            }



            if (this.keyQ.isDown) {
                game.state.start('rank');
                this.game_sound.stop();
            }
            if (this.keyA.isDown) {
                game.state.start('play');
                this.game_sound.stop();
            }

            this.movePlayer();
            if (this.gamepause.isDown) {
                game.paused = true;
            }
////////////////////////////
            if(this.keyZ.isDown&&game.global.counter%10==0&&game.global.counter!==0){
                this.helper.reset(game.width,game.height);
                this.helper.body.velocity.y=-50;
                this.helper.body.velocity.x=-100;
                game.global.counter++;
                this.coinLabel.text = 'COIN: ' + game.global.counter;
            }
            
 ////////////////////////////////////////       
        
        
        //console.log(game.global.pau);

    }, // No changes
    movePlayer: function () {
        var _this=this;
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200-game.global.counter*3;
            this.player.animations.play('fly');
        }

        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = 200+game.global.counter*3;
            this.player.animations.play('fly');
        }
        else if (this.cursor.up.isDown) {
            this.player.body.velocity.y = -200-game.global.counter*3;
            this.player.animations.play('fly');
        }
        else if (this.cursor.down.isDown) {
            this.player.body.velocity.y = 200+-game.global.counter*3;
            this.player.animations.play('fly');
        }

        else {
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y=0;
            this.player.animations.play('no_walk');
        }
        if (fireButton.isDown) {
            if(game.global.ultcnt == 5){
                this.weapon.fireRate = 0;
                this.weapon.fire({ x: _this.player.x+25, y: _this.player.y });
                this.weapon.fire({ x: _this.player.x + 35, y: _this.player.y });
                this.weapon.fire({ x: _this.player.x + 45, y: _this.player.y });
                this.weapon.fireRate = 1000;
                this.lazer.play();
                game.time.events.add(Phaser.Timer.SECOND * 5, function(){
                    game.global.ultcnt=0;
                });
            }
            else{
                this.weapon.fireRate=100;
                this.weapon.fire();
                this.weap.fire();
            }
           
            this.lazer.play();
        }
       
        //console.log(this.player.body.velocity.y);
    }, // No changes 
    takeCoin: function (player, coin) {
        // Use the new score variable game.global.score += 5;
        // Use the new score variable
        game.global.score += 5;
        game.global.counter++;
        this.scoreLabel.text = 'score: ' + game.global.score;
        this.coinLabel.text = 'COIN: ' + game.global.counter;
        // Then no changes
        var coinPosition = [{ x: 140, y: 60 }, { x: 440, y: 60 },
        { x: 100, y: 320 }, { x: 840, y: 140 },
        { x: 900, y: 300 }, { x: 780, y: 600 }];
        for (var i = 0; i < coinPosition.length; i++) {
            if (coinPosition[i].x == this.coin.x) {
                coinPosition.splice(i, 1);
            }
        }
        var newPosition = game.rnd.pick(coinPosition);
        coin.reset(newPosition.x, newPosition.y);
    },
    createParticle: function (pointer, doubleTap) {
        var emitter = this.game.add.emitter(this.player.x + 10, this.player.y + 20, 200);
        emitter.width = 10;
        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-100, 100);
        emitter.particleClass = CoinParticle;
        emitter.makeParticles();
        emitter.gravity = 200;
        emitter.start(true, 3000, null, 50);
    },
    /* createParticle_enemy: function (bullet,enemy) {
         var emitter = this.game.add.emitter(enemy.x + 10, enemy.y + 20, 200);
         emitter.width = 10;
         emitter.setXSpeed(-100, 100);
         emitter.setYSpeed(-100, 100);
         emitter.particleClass = CoinParticle;
         emitter.makeParticles();
         emitter.gravity = 200;
         emitter.start(true, 3000, null, 50);
     },*/
    //updateCoinPosition: function() { }, // No changes
    addEnemy: function () {

        var enemy = this.enemies.getFirstDead();
        if (!enemy) { return; }
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.width / 2, 0);
        if (game.global.level == 1) {
            enemy.body.velocity.y = 100;
            enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
        }
        else {
            enemy.body.velocity.y = 400;
            enemy.body.velocity.x = 200 * game.rnd.pick([-1, 1]);
        }

        enemy.body.bounce.x = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    }, // No changes
    /*  addEnemys: function () {
          var enemy2 = this.enemys.getFirstDead();
          if (!enemy2) { return; }
          enemy2.anchor.setTo(0.5, 1);
          enemy2.reset(game.width / 4, 0);
          if(game.global.level==1){
              enemy2.body.velocity.y = 100;
              enemy2.body.velocity.x = 100 * game.rnd.pick([-2, -1, 1, 2]);
          }
          else{
              enemy2.body.velocity.y = 300;
              enemy2.body.velocity.x = 200 * game.rnd.pick([-2, -1, 1, 2]);
          }
          
          enemy2.body.bounce.x = 1;
          enemy2.checkWorldBounds = true;
          enemy2.outOfBoundsKill = true;
      },*/
    playerDie: function (player, bullet) {

        bullet.kill();
        if (game.global.hp > 0) {
            game.global.hp--;
            if (game.global.score >= 50)
                game.global.score -= 50;
            else
                game.global.score = 0;
            this.scoreLabel.text = 'score: ' + game.global.score;
            this.createParticle();
            this.lifeLabel.text = 'LIFE: ' + game.global.hp;
            //console.log(game.global.hp);
            this.player.reset(game.width / 2, game.height / 2);
        }
        else {
            game.state.start('rank');
            this.game_sound.stop();
        }
        //enemyBullet.kill();  
    },
    playerDie2: function () {
        // When the player dies, go to the menu

        if (game.global.hp > 0) {
            game.global.hp--;
            if (game.global.score >= 50)
                game.global.score -= 50;
            else
                game.global.score = 0;
            this.scoreLabel.text = 'score: ' + game.global.score;
            this.lifeLabel.text = 'LIFE: ' + game.global.hp;
            console.log(game.global.hp);
            this.player.reset(game.width / 2, game.height / 2);
        }
        else {
            game.state.start('rank');
            this.game_sound.stop();
        }
    },
    EnemyDie: function (bullet, enemy) {
        game.global.score += 100;
        if (game.global.ultcnt < 5)
            game.global.ultcnt++;
        this.mp.width = game.global.ultcnt * 40 + 3;
        //this. createParticle_enemy();
        var emitter = this.game.add.emitter(enemy.x + 10, enemy.y + 20, 200);
        emitter.width = 10;
        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-100, 100);
        emitter.particleClass = CoinParticle;
        emitter.makeParticles();
        emitter.gravity = 200;
        emitter.start(true, 3000, null, 50);
        this.scoreLabel.text = 'score: ' + game.global.score;
        enemy.kill();
        //this.enemies.remove(enemy);
        bullet.kill();

    }


};
        // Delete all Phaser initialization code