// Initialize Phaser
var game = new Phaser.Game(1000, 680, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 , level:1};
// Add all the states
game.state.add('play', playState);
//game.state.add('play2', playState2);
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('rank', rankState);

// Start the 'boot' state
game.state.start('boot');