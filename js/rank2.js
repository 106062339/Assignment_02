var sprite;
var weapon;
var cursors;
var fireButton;
var astr_falling = true;
var name = true;
var rankState = {
    preload:function(){
       
        
    },
    create:function(){
        
        this.cursor = game.input.keyboard.createCursorKeys();

        this.background=game.add.image(0, 0, 'background');
        //menu text
        this.background.width = game.width;
        this.background.height = game.height;
        var style = {fill: 'black', fontSize: '18px'};
        var title_style = {fill: 'black', fontSize: '40px'};
        this.NS = game.add.text(game.width/2, 50, 'Ranking', title_style);
        
        this.index = game.add.text(140, 360, 'PRESS SPACE TO MENU', style);
        this.index2=game.add.text(140, 380, 'PRESS A TO PLAY AGIAN', style);
        
        this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
       
        
        if ( game.global.score) {
            var Ref = firebase.database().ref('score_list2');
            var data = {
               // name: game.global.player_name,
                score : Math.round(game.global.score)
            }
            Ref.push(data);
            var text2 = game.add.text(600, 350, 'Your Score: ' + Math.round(game.global.score));
        }
        
        
        //firebase output
        var postsRef = firebase.database().ref('score_list2').orderByChild('score').limitToLast(5);

        var num = [];
        var user = [];
        var place = 1;
        var style2 = {fill: 'white', fontSize: '20px'}
        postsRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                num.push(childSnapshot.val().score);
                //user.push(childSnapshot.val().name);
            });
          }).then(function(style){
            for(var i=4; i>=0; i--){
                if(num[i])
                {
                    game.add.text(700, 250-i*30, 'N0.' + place.toString() + '    '  + num[i].toString(), style).anchor.setTo(0, 0.5);
                    place++;
                }
            }
        });

        //this.gameoverSound = game.add.audio('gameOver');
        //this.gameoverSound.play();
        
    },
    update:function(){

      
        var keyboard3 = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        if(keyboard3.isDown){
            game.state.start('menu');
        }
        if(this.keyQ.isDown){
            game.state.start('rank2');
        }
        if(this.keyA.isDown){
            game.state.start('play');
        }

    }

    
};