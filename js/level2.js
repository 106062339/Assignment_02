var weapon;
var enemyBulletTime = 0;
var livingEnemies = [];
var playerShipExplosion;
var enemyShotSound;
var playerShotSound;
var pause;
var mute;
var CoinParticle = (function () {
    var CoinParticle = function (game, x, y) {
        Phaser.Particle.call(this, game, x, y, 'coin_dead');
        this.animations.add("rotate");
    };
    CoinParticle.prototype = Object.create(Phaser.Particle.prototype);
    CoinParticle.prototype.constructor = CoinParticle;
    CoinParticle.prototype.onEmit = function () {
        this.animations.stop("rotate", true);
        this.animations.play("rotate", 60, true);
        this.animations.getAnimation('rotate').frame = Math.floor(Math.random() * this.animations.getAnimation('rotate').frameTotal);
    }
    return CoinParticle;
}());

var playState2 = {
    preload: function () { }, // The proload state does nothing now.
    create: function () {
        //weapon&music
        this.lazer = game.add.audio('bullet_sound');
        this.game_sound = game.add.audio('game_sound');
        this.game_sound.loopFull();
        var _this=this;
        this.bg = game.add.sprite(0, -1*game.height, 'bg');
        this.bg.width = game.width;
        this.bg.height = game.height*2;
        game.physics.enable(this.bg, Phaser.Physics.ARCADE);
        //particle
        this.game.input.onTap.add(this.createParticle, this);

        //keys
        this.keyP = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.keyO = game.input.keyboard.addKey(Phaser.Keyboard.O);
        this.keyM = game.input.keyboard.addKey(Phaser.Keyboard.M);
        this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.keyplus = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        this.keyminus = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        this.mute = false;

        this.keyP.onDown.add(function () {
            _this.game_sound.pause();
            pause = true;
        });
        this.keyO.onDown.add(function () {
            _this.game_sound.resume();
            pause = false;
        });
        this.keyM.onDown.add(function () {
            if (mute == false) {
                _this.game_sound.mute = true;
                mute = true;
            }
            else {
                _this.game_sound.mute = false;
                mute = false;
            }

        });
        this.keyplus.onDown.add(function () {
            //console.log(game_sound.volume);
            _this.game_sound.volume += 0.1;
            if (_this.game_sound.volume > 1) _this.game_sound.volume = 1;
        });
        this.keyminus.onDown.add(function () {
            //console.log(game_sound.volume);
            _this.game_sound.volume -= 0.1;
            if (_this.game_sound.volume < 0) _this.game_sound.volume = 0;
        });




        game.global.score = 0;
        game.global.hp=3;
        this.cursor = game.input.keyboard.createCursorKeys();
        this.player = game.add.sprite(game.width / 2, game.height / 2, 'player');
        game.physics.arcade.enable(this.player);
        this.player.frame = 23;
        this.player.animations.add('no_walk', [0, 1, 2, 3, 4, 5, 6, 7], 8, true);
        this.player.animations.add('left_walk', [8, 9, 10, 11, 12, 13, 14, 15], 8, true);
        this.player.animations.add('right_walk', [16, 17, 18, 19, 20, 21, 22, 23], 8, true);
    

        this.walls = game.add.group();
        this.walls.enableBody = true;
        //game.add.sprite(0, 0, 'wallV', 0, this.walls);
        this.walls.setAll('body.immovable', true);

        this.coin = game.add.sprite(60, 140, 'coin');
        game.physics.arcade.enable(this.coin);
        this.coin.anchor.setTo(0.5, 0.5);
        this.scoreLabel = game.add.text(700, 400, 'score: 0',
            { font: '30px Arial', fill: '#ffffff' });
        this.lifeLabel = game.add.text(700, 450, 'LIFE: '+game.global.hp,
            { font: '30px Arial', fill: '#ffffff' });
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');
        game.time.events.loop(2500, this.addEnemy, this);

        //  Creates 1 single bullet, using the 'bullet' graphic
        this.weapon = game.add.weapon(10, 'bullet1');
        //  The bullet will be automatically killed when it leaves the world bounds
        game.physics.arcade.enable(this.weapon);
        this.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        //  Because our bullet is drawn facing up, we need to offset its rotation:
        this.weapon.bulletAngleOffset = 90;
        //  The speed at which the bullet is fired
        this.weapon.bulletSpeed = 400;

        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'bullet2');
        this.enemyBullets.setAll('anchor.x', -1);
        this.enemyBullets.setAll('anchor.y', 0);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        //  Tell the Weapon to track the 'player' Sprite, offset by 14px horizontally, 0 vertically 
        this.weapon.trackSprite(this.player, 25, 0);
        //weapon.trackSprite(this.enemies, 100, 600);
        cursors = this.input.keyboard.createCursorKeys();
        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
    },
    update: function () {

        //console.log(this.enemies.countLiving());
        if (!this.player.inWorld) { this.playerDie2(); }
        game.physics.arcade.collide(this.player, this.floor);
        game.physics.arcade.collide(this.player, this.walls);
        game.physics.arcade.collide(this.enemies, this.walls);
        game.physics.arcade.collide(this.enemies, this.floor);
        game.physics.arcade.collide(this.enemies, this.weapon);
        game.physics.arcade.overlap(this.weapon.bullets, this.enemies, this.EnemyDie, null, this);
        game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.playerDie2, null, this);
        game.physics.arcade.overlap(this.enemyBullets, this.player, this.playerDie, null, this);

        livingEnemies.length = 0;
        this.enemies.forEachAlive(function (enemy) {
            livingEnemies.push(enemy)
        });

        if (this.time.now > enemyBulletTime) {
            this.enemyBullet = this.enemyBullets.getFirstExists(false);
            if (this.enemyBullet && livingEnemies.length > 0) {

                var random = this.rnd.integerInRange(0, livingEnemies.length - 1);
                var shooter = livingEnemies[random];
                this.enemyBullet.reset(shooter.body.x, shooter.body.y + 70);
                enemyBulletTime = this.time.now + 500;
                this.enemyBullet.body.velocity.y = 300;
            }
        }

        this.bg.body.velocity.y=100;
        if(this.bg.y>=-1){
            this.bg.y=-1*game.height;
        }

        if(this.keyQ.isDown){
            game.state.start('rank');
        }
        if(this.keyA.isDown){
            game.state.start('play');
        }

        this.movePlayer();
    }, // No changes
    movePlayer: function () {
        if (this.cursor.left.isDown){
            this.player.body.velocity.x = -200;
            this.player.animations.play('left_walk');
        }
            
        else if (this.cursor.right.isDown){
            this.player.body.velocity.x = 200;
            this.player.animations.play('right_walk');
        }            
        else if (this.cursor.up.isDown){
            this.player.body.velocity.y = -200;
            this.player.animations.play('left_walk');
        }            
        else if (this.cursor.down.isDown){
            this.player.body.velocity.y = 200;
            this.player.animations.play('no_walk');
        }
            
        else {
            this.player.body.velocity.x = 0;
            this.player.animations.play('no_walk');
        }
        if (fireButton.isDown) {
            this.weapon.fire();
            this.lazer.play();
        }
        //console.log(this.player.body.velocity.y);
    }, // No changes 
    takeCoin: function (player, coin) {
        // Use the new score variable game.global.score += 5;
        // Use the new score variable
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;

        // Then no changes
        var coinPosition = [{ x: 140, y: 60 }, { x: 360, y: 60 },
        { x: 60, y: 140 }, { x: 440, y: 140 },
        { x: 130, y: 300 }, { x: 370, y: 300 }];
        for (var i = 0; i < coinPosition.length; i++) {
            if (coinPosition[i].x == this.coin.x) {
                coinPosition.splice(i, 1);
            }
        }
        var newPosition = game.rnd.pick(coinPosition);
        coin.reset(newPosition.x, newPosition.y);
    },
    createParticle: function (pointer, doubleTap) {
        var emitter = this.game.add.emitter(this.player.x + 10, this.player.y + 20, 200);
        emitter.width = 10;
        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-100, 100);
        emitter.particleClass = CoinParticle;
        emitter.makeParticles();
        emitter.gravity = 200;
        emitter.start(true, 3000, null, 50);
    },
    //updateCoinPosition: function() { }, // No changes
    addEnemy: function () {
        var enemy = this.enemies.getFirstDead();
        if (!enemy) { return; }
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.width / 2, 0);
        enemy.body.velocity.y = 100;
        enemy.body.velocity.x = 100 * game.rnd.pick([-2, -1, 1, 2]);
        enemy.body.bounce.x = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    }, // No changes
    playerDie: function (player,bullet) {
        
        bullet.kill();
        if(game.global.hp>0){
            game.global.hp--;
            this.lifeLabel.text = 'LIFE: ' + game.global.hp;
            console.log(game.global.hp);
            this.player.reset(game.width/2,game.height/2);
        }
        else {
             game.state.start('rank');
             this.game_sound.stop();
        }
        //enemyBullet.kill();  
    },
    playerDie2: function () {
        // When the player dies, go to the menu

        if(game.global.hp>0){
            game.global.hp--;
            this.lifeLabel.text = 'LIFE: ' + game.global.hp;
            console.log(game.global.hp);
            this.player.reset(game.width/2,game.height/2);
        }
        else {
             game.state.start('rank');
             this.game_sound.stop();
        }
    },
    EnemyDie: function (bullet, enemy) {
        game.global.score+=100;
        enemy.kill();
        //this.enemies.remove(enemy);
        bullet.kill();

    }


};
        // Delete all Phaser initialization code