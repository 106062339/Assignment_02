# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 在目錄選擇左箭頭可到達level 1地圖 選擇右箭頭可到level 2地圖  有子彈速度、敵人移動速度、地圖的差異   攻擊對手增加MP  集滿持續點擊空白鍵可使用大絕 
2. Animations : player移動時會有飛行畫面
3. Particle Systems :player 和 enemy被打到都會撒錢
4. Sound effects : 遊戲進行中  以及  發射子彈都會有聲效  遊戲過程中 按L音量降  按K音量增 按P靜音  按O放音
5. Leaderboard : 遊戲失敗的game over畫面有用firebase紀錄的ranking 含名字
6. UI   用紅條紀錄ultimate counter  遊戲中按* 可暫停 按空白鍵繼續 右下方紀錄HP、分數
7. Complete game process: 在menu點擊左右選擇Level進入遊戲  生命耗盡則跳出game over 畫面(包含ranking) 在game over畫面按A重新遊戲 空白鍵回到menu
8. 生命減少時會將玩家回到正中央

# Bonus Functions Description : 
1. 金幣  只要吃到金幣 角色的移動速度會隨著吃越多越快
2. helper  只要吃到10的倍數個金幣 點擊Z即可從右下角叫出helper  只要我們攻擊  該helper也會幫忙攻擊